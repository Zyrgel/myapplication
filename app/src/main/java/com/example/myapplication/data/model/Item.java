package com.example.myapplication.data.model;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor
public class Item {
    Long id;
    String title;
    String description;
    BigDecimal price;

    List<String> photos = new ArrayList<>();
    /*
    //TODO: Add users
    User seller
    User buyer
     */

    public Item(JSONObject job) throws JSONException{
        setId(job.getLong("id"));
        setTitle(job.getString("title"));
        setDescription(job.getString("description"));
        setPrice(new BigDecimal(job.getString("price")));

        if(job.has("photos")){
            JSONArray photos = job.getJSONArray("photos");
            for(int i = 0; i < photos.length(); i++){
                JSONObject photo = photos.getJSONObject(i);
                this.photos.add(photo.getString("subpath"));
            }
        }
    }
}
