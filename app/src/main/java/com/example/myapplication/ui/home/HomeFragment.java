package com.example.myapplication.ui.home;


import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;


public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        RecyclerView view = (RecyclerView) inflater.inflate(R.layout.fragment_home_list, container, false);
        view.setLayoutManager(new LinearLayoutManager(view.getContext()));
        HomeViewModel model = ViewModelProviders.of(this.getActivity()).get(HomeViewModel.class);
        model.getItems().observe(this, items ->
                view.setAdapter(new ItemRecyclerViewAdapter(items)));
        return view;
    }
}