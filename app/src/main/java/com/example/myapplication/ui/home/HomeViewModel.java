package com.example.myapplication.ui.home;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.myapplication.data.model.Item;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class HomeViewModel extends AndroidViewModel {

    MutableLiveData<List<Item>> items;
    MutableLiveData<Item> selected = new MutableLiveData<>();

    RequestQueue requestQueue;

    public HomeViewModel(Application context) {
        super(context);
        requestQueue = Volley.newRequestQueue(context);
    }

    public LiveData<List<Item>> getItems(){
        if(items == null){
            items = new MutableLiveData<>();
            loadItems();
        }
        return items;
    }

    protected void loadItems() {
        String url = "http://158.38.101.138/api/fant";
        JsonArrayRequest jar = new JsonArrayRequest(Request.Method.GET, url, null,
                response -> {
                    List<Item> items = new ArrayList<>();
                    try {
                        for(int i = 0; i < response.length(); i++){
                            items.add(new Item(response.getJSONObject(i)));
                        }
                    } catch (JSONException jex){
                        System.out.println(jex);
                    }
                    this.items.setValue(items);
                }, System.out::println);
        requestQueue.add(jar);
    }

    public MutableLiveData<Item> getSelected() {
        return selected;
    }

    public void setSelected(Item selected) {
        this.selected.setValue(selected);
    }
}