package com.example.myapplication.ui.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.example.myapplication.R;
import com.squareup.picasso.Picasso;

public class itemDetailsFragment extends Fragment {

    ImageView photo;
    TextView description;
    TextView price;

    @Override
    public void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
        HomeViewModel model = ViewModelProviders.of(this.getActivity()).get(HomeViewModel.class);
        model.getSelected().observe(this, item -> {
            String url = "http://158.38.101.138/api/fant/photo/" + item.getPhotos().get(0);
            Picasso.get().load(url).into(photo);
            ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(item.getTitle());
            description.setText(item.getDescription());
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View result = inflater.inflate(R.layout.fragment_item_detail, container, false);

        photo = result.findViewById(R.id.photo);
        description = result.findViewById(R.id.description);
        price = result.findViewById(R.id.price);

        return result;
    }

}
